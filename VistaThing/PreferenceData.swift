//
//  PreferenceData.swift
//  VistaThing
//
//  A singleton class to get and set preference data for the app.
//
//  Created by Linda Thomas on 11/6/14.
//  Copyright (c) 2014 Linda Thomas. All rights reserved.
//

import Foundation

private let _standardPrefs = PreferenceData()

class PreferenceData {

    private var _smartAppClientID : String?
    private var _smartAppClientSecret : String?
    private var _smartAppAccessToken : String?
    private var _vistaSerialDevice : String?
    private var _vistaCode : String?
    
    var smartAppClientID : String? {
        set {
            _smartAppClientID = newValue
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: "smartAppClientID")
        }
        get {
            if _smartAppClientID == nil {
                _smartAppClientID = NSUserDefaults.standardUserDefaults().valueForKey("smartAppClientID") as? String
            }
            return _smartAppClientID
        }
    }
    
    var smartAppClientSecret : String? {
        set {
            _smartAppClientSecret = newValue
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: "smartAppClientSecret")
        }
        get {
            if _smartAppClientSecret == nil {
                _smartAppClientSecret = NSUserDefaults.standardUserDefaults().valueForKey("smartAppClientSecret") as? String
            }
            return _smartAppClientSecret
        }
    }

    var smartAppAccessToken : String? {
        set {
            _smartAppAccessToken = newValue
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: "smartAppAccessToken")
        }
        get {
            if _smartAppAccessToken == nil {
                _smartAppAccessToken = NSUserDefaults.standardUserDefaults().valueForKey("smartAppAccessToken") as? String
            }
            return _smartAppAccessToken
        }
    }

    var vistaSerialDevice : String? {
        set {
            _vistaSerialDevice = newValue
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: "vistaSerialDevice")
        }
        get {
            if _vistaSerialDevice == nil {
                _vistaSerialDevice = NSUserDefaults.standardUserDefaults().valueForKey("vistaSerialDevice") as? String
            }
            return _vistaSerialDevice
        }
    }
    
    var vistaCode : String? {
        set {
            _vistaCode = newValue
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: "vistaCode")
        }
        get {
            if _vistaCode == nil {
                _vistaCode = NSUserDefaults.standardUserDefaults().valueForKey("vistaCode") as? String
            }
            return _vistaCode
        }
    }
    
    class func standardDefaults() -> PreferenceData {
        return _standardPrefs
    }

    init() {
    }
    
}