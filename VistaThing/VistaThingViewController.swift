//
//  ViewController.swift
//  VistaThing
//
//  Created by Linda Thomas on 10/24/14.
//  Copyright (c) 2014 Linda Thomas. All rights reserved.
//

import Cocoa

class VistaThingViewController: NSViewController, AKSmartThingsDelegate, ORSSerialPortDelegate {

    //MARK: hardcoded zone configuration for now
    struct ZoneInfo {
        var zone : String
        var name : String
        var type: String
        var getsFaultMessages: Bool  // anything that gets fault messages also gets check and lo bat messages
    }
    let definedZones = [
        ZoneInfo(zone: "ALM",name: "Alarm",                         type: "VistaThing Alarm",               getsFaultMessages: false),
        ZoneInfo(zone: "CTL",name: "Alarm Controller",              type: "VistaThing Alarm Controller",    getsFaultMessages: false),
        ZoneInfo(zone: "CHM",name: "Alarm Chime",                   type: "VistaThing Chime",               getsFaultMessages: false),
        ZoneInfo(zone: "PWR",name: "Alarm Power Status",            type: "VistaThing Power Status Sensor", getsFaultMessages: false),
        ZoneInfo(zone: "01", name: "Front Door",                    type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "03", name: "Basement Door",                 type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "04", name: "Exercise Room Left Window",     type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "05", name: "Exercise Room Right Window",    type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "06", name: "Rec Room Left Window",          type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "07", name: "Rec Room Right Window",         type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "08", name: "Interior Garage Door",          type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "10", name: "Library Glass Break",           type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "11", name: "Dining Room Glass Break",       type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "12", name: "1st Floor Office Glass Break",  type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "13", name: "Family Room Glass Break",       type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "14", name: "Kitchen Glass Break",           type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "15", name: "Master Bedroom Glass Break",    type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "16", name: "Green Bedroom Glass Break",     type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "17", name: "2nd Floor Office Glass Break",  type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "18", name: "Purple Bedroom Glass Break",    type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "20", name: "Guest Bathroom Window",         type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "21", name: "Central Foyer",                 type: "VistaThing Motion Sensor",       getsFaultMessages: true),
        ZoneInfo(zone: "22", name: "Basement Bedroom Window",       type: "VistaThing Contact Sensor",      getsFaultMessages: true),
        ZoneInfo(zone: "23", name: "Basement Smoke Detector",       type: "VistaThing Smoke Detector",      getsFaultMessages: true),
        ZoneInfo(zone: "24", name: "2nd Floor Hall Smoke Detector", type: "VistaThing Smoke Detector",      getsFaultMessages: true),
        ZoneInfo(zone: "25", name: "Kitchen Door",                  type: "VistaThing Contact Sensor",      getsFaultMessages: true)
    ]
    var zoneDict = [String:ZoneInfo]()
    
    //MARK: counters for sensor create and delete
    var createIndex = 0
    var deleteIndex = 0
    
    //MARK: alarm siren status
    enum AlarmStatus {
        case off
        case alarm
        case fire
    }
    var alarmStatus : AlarmStatus?
    
    //MARK: chime status
    var chimeOn : Bool?
    
    //MARK: alarm panel power status
    var panelOnWallPower : Bool?
    
    //MARK: arming status
    enum ArmMode {
        case disarmed
        case armAway
        case armStay
        case testMode
    }
    var armMode : ArmMode?
    
    //MARK: sensor status
    enum SensorStatus {
        case ok
        case lowBattery
        case check
        case bogus   // for inital setting before updates
    }
    var panelBatteryStatus = SensorStatus.bogus

    //MARK: vista messages
    var showAllVistaMessages = false
    
    //MARK: attribute names and value for sensor types
    struct Attribute {
        var attributeName : String
        var faultedAttributeValue : String
        var clearAttributeValue : String
    }
    
    //MARK: used whend processing fault messages to send to the appropriate attribute to SmartThings
    let sensorAttributes = [
        "VistaThing Contact Sensor" : Attribute(attributeName: "contact", faultedAttributeValue: "open", clearAttributeValue: "closed"),
        "VistaThing Motion Sensor" : Attribute(attributeName: "motion", faultedAttributeValue: "active", clearAttributeValue: "inactive"),
        "VistaThing Smoke Detector" : Attribute(attributeName: "smoke", faultedAttributeValue: "detected", clearAttributeValue: "clear")
   ]
    
    //MARK: current state of Vista sensors
    struct CurrentSensorState {
        var attributeName : String
        var attributeState : String
        var sensorStatus: SensorStatus
        var lastUpdateTime : NSTimeInterval
    }
    var zoneState = [String:CurrentSensorState]()
    
    //MARK: convenience properties for accessing preference data
    var vistaCode : String? {
        get {
            return PreferenceData.standardDefaults().vistaCode
        }
    }
    
    //MARK: ui outlets
    @IBOutlet var log: NSTextView!
    @IBOutlet weak var connectToSmartTingsButton: NSButton!
    @IBOutlet var topLevelView: NSView!
    
    //MAR: http server stuff
    var http : RoutingHTTPServer?
    
    //MARK: our ip address to send to ST
    var ipAddress = ""
    
    //MARK: AKSMartThings oauth stuff
    var thingie : AKSmartThings?
    var accessToken : String?
    
    //MARK: serial port for Vista
    var serialPort : ORSSerialPort?
    var lineBuffer : String = ""
    
    //MARK: date formatter for log
    let dateFormatter = NSDateFormatter()
    
    //MARK: view controller
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateFormatter.setLocalizedDateFormatFromTemplate("yyyy-MM-dd HH:mm:ss")
        self.setupHTTPServer()
        self.thingie = AKSmartThings(server: self.http!)
        self.thingie?.delegate = self
        self.thingie?.clientId = PreferenceData.standardDefaults().smartAppClientID
        self.thingie?.clientSecret = PreferenceData.standardDefaults().smartAppClientSecret
        self.thingie?.accessToken = PreferenceData.standardDefaults().smartAppAccessToken
        for zoneInfo in self.definedZones {
            zoneDict[zoneInfo.zone] = zoneInfo
            zoneState[zoneInfo.zone] = CurrentSensorState(attributeName: "", attributeState: "", sensorStatus: SensorStatus.bogus, lastUpdateTime: 0)
        }
        connectToSmartThings()
    }

    var firstAppearance = true
    override func viewDidAppear() {
        super.viewDidAppear()
        if firstAppearance {
            topLevelView.window?.miniaturize(self)
            firstAppearance = false
        }
    }
    
    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    //MARK: ui actions
    
    @IBAction func connectToSmartThings(sender: NSButton) {
        connectToSmartThings()
     }

    @IBAction func sendZones(sender: NSButton) {
        if PreferenceData.standardDefaults().smartAppAccessToken != nil {
            self.createSensor(createIndex)
         }
    }
    
    @IBAction func deleteZones(sender: NSButton) {
        if PreferenceData.standardDefaults().smartAppAccessToken != nil {
            deleteSensor(deleteIndex)
        }
    }
    
    @IBAction func listSensors(sender: NSButton) {
        self.thingie?.getJSONFor("sensors", withCallback: "listSensorResults:")
    }
    
    @IBAction func listenToVista(sender: NSButton) {
        listenToVista()
    }
    
    @IBAction func stopListeningToVista(sender: NSButton) {
        stopListeningToVista()
    }
    
    @IBAction func toggleVistaMessages(sender: NSButton) {
        showAllVistaMessages = !showAllVistaMessages
        appendToLog("Show all Vista messages: \(showAllVistaMessages)")
    }
    
    private func appendToLog(msg: String) {
        let d = dateFormatter.stringFromDate(NSDate())
        let s = "\(d)) \(msg)\n"
        let attributedMsg = NSAttributedString(string: s)
         dispatch_async(dispatch_get_main_queue()) {
            [unowned self] in
            let tv = self.log
            let oldString = tv.string!
            if oldString.characters.count > 11000 {
                let newString = oldString.substringFromIndex(oldString.startIndex.advancedBy(1000)) + msg
                tv.textStorage?.setAttributedString(NSAttributedString(string: newString))
            }
            else {
                tv.textStorage?.appendAttributedString(attributedMsg)
            }
            tv.scrollRangeToVisible(NSMakeRange(tv.string!.characters.count, 0))
        }
    }
    
    //MARK: implementation of UI commands
    
    func connectToSmartThings() {
        if PreferenceData.standardDefaults().smartAppAccessToken == nil {
            if let id = PreferenceData.standardDefaults().smartAppClientID {
                self.thingie?.clientId = id
                if let secret = PreferenceData.standardDefaults().smartAppClientSecret {
                    self.thingie?.askUserForPermissionUsingClientSecret(secret)
                }
            }
        }
    }
    
    func listenToVista() {
        stopListeningToVista()
         if let serialPath = PreferenceData.standardDefaults().vistaSerialDevice {
            serialPort = ORSSerialPort(path: serialPath)
            serialPort?.baudRate = 115200
            serialPort?.delegate = self
            serialPort?.open()
            appendToLog("Listening to Vista alarm panel")
        }
    }
    
    func stopListeningToVista() {
        if let port = serialPort {
            port.close()
            serialPort = nil
            appendToLog("No longer listening to Vista alarm panel")
        }
    }
    
    func createSensor(definedSensorIndex: Int) {
        let zoneInfo = self.definedZones[createIndex]
        let sensor = ["zone":zoneInfo.zone, "type":zoneInfo.type, "name":zoneInfo.name]
        //var error : NSError?
        self.thingie?.getJSONForPOST("createSensor", withData:sensor, withCallback: "sensorCreationResults:")
    }
    
    func deleteSensor(definedSensorIndex: Int) {
        //var error : NSError?
        let zoneInfo = self.definedZones[deleteIndex]
        let id = "VistaThing \(zoneInfo.zone)"
        let requestData = ["id": id]
        self.thingie?.getJSONForPOST("deleteSensor", withData:requestData, withCallback: "sensorDeleteResults:")
    }

    func updateSensor(zone: String, attribute: String, value: String) {
        self.thingie?.getJSONFor("sensor/\(zone)/\(attribute)/\(value)", withCallback: "sensorUpdated:")
    }
    
    //MARK: callbacks for server requests
    
    func sensorCreationResults(json: AnyObject?) {
        appendToLog("in sensorCreationResults")
        if json == nil {
            appendToLog("  no result returned")
        }
        else {
            appendToLog("  return result was \(json)")
        }
        if ++createIndex < self.definedZones.count {
            createSensor(createIndex)
        }
        else {
            appendToLog("finished with sensor creation requests")
            createIndex = 0
        }
    }
    
    func sensorDeleteResults(json: AnyObject?) {
        appendToLog("in sensorDeleteResults")
        if json == nil {
            appendToLog("  no result returned")
        }
        else {
            appendToLog("  return result was \(json)")
        }
        if ++deleteIndex < self.definedZones.count {
            deleteSensor(deleteIndex)
        }
        else {
            appendToLog("finished with sensor delete requests")
            deleteIndex = 0
        }
   }
    
    func listSensorResults(json: AnyObject?) {
        appendToLog("in listSensorResults")
        if json == nil {
            appendToLog("  no result returned")
        }
        else {
            appendToLog("  return result was \(json)")
        }
    }
    
    func sensorUpdated(json: AnyObject?) {
        appendToLog("in sensorUpdated")
        if json == nil {
            appendToLog("  no result returned")
        }
        else {
            appendToLog("  return result was \(json)")
        }
    }

    func chimeUpdated(json: AnyObject?) {
        appendToLog("in chimeUpdated")
        if json == nil {
            appendToLog("  no result returned")
        }
        else {
            appendToLog("  return result was \(json)")
        }
    }
    
    func armModeUpdated(json: AnyObject?) {
        appendToLog("in armModeUpdated")
        if json == nil {
            appendToLog("  no result returned")
        }
        else {
            appendToLog("  return result was \(json)")
        }
    }
    
    func messageResults(json: AnyObject?) {
//        appendToLog("in messageResults")
//        if json == nil {
//            appendToLog("  no result returned")
//        }
//        else {
//            appendToLog("  return result was \(json)")
//        }
    }
    
    func clientIPCallback(json: AnyObject?) {
        appendToLog("in clientIPCallback")
        if json == nil {
            appendToLog("  no result returned")
        }
        else {
            appendToLog("  return result was \(json)")
        }
        listenToVista()
    }
    

    //MARK: http server code
    
    private func setupHTTPServer() {
        var appVersion : String
        var appName : String
        
        if let bundleInfo = NSBundle.mainBundle().infoDictionary {
            if let av = bundleInfo["CFBundleShortVersionString"] as? String {
                appVersion = av
            }
            else if let av = bundleInfo["CFBundleVersion"] as? String {
                appVersion = av
            }
            else {
                appVersion = "0.0"
            }
            if let an = bundleInfo["CFBundleName"] as? String {
                appName = an
            }
            else {
                appName = "VistaThing"
            }
        }
        else {
            appName = "VistaThing"
            appVersion = "0.0"
        }
        let serverHeader = "\(appName)/\(appVersion)"
        
        let addresses = NSHost.currentHost().addresses
        for address in addresses as [String] {
            let r = address.rangeOfString(".")?.startIndex
            if self.ipAddress == "" && r != nil {
                self.ipAddress = address
                appendToLog("server address: \(address)")
                break
            }
        }

       // var error : NSError?
        http = RoutingHTTPServer()
        http?.setPort(9876)
        http?.setDefaultHeader("server", value: serverHeader)
        setupRoutes()
        http?.setName("")
        http?.setType("_adtVista._tcp")
        do {
            try http?.start()
        } catch {
            print("error on http start \(error)")
        }
        appendToLog("port number is \(http!.port())")
        
    }

    private func setupRoutes() {
        
        http?.get("/toggleChime") { [unowned self] (request, response) in
            self.appendToLog("toggle chime called")
            if let code = self.vistaCode {
                let cmd = "\(code)9"
                let data = (cmd as NSString).dataUsingEncoding(NSUTF8StringEncoding)
                self.serialPort?.sendData(data)
                response.respondWithString("chime command sent")
            }
            else {
                self.appendToLog("Alarm code has not been set.")
            }
        }
        
        http?.get("/disarm") { [unowned self] (request, response) in
            self.appendToLog("disarm called")
            if let code = self.vistaCode {
                self.sendToVista("\(code)1")
                response.respondWithString("disarm command sent")
            }
            else {
                self.appendToLog("Alarm code has not been set.")
            }
        }
   
        http?.get("/armAway") { [unowned self] (request, response) in
            self.appendToLog("armAway called")
            if let code = self.vistaCode {
                self.sendToVista("\(code)2")
                response.respondWithString("armAway command sent")
            }
            else {
                self.appendToLog("Alarm code has not been set.")
            }
        }

        http?.get("/armStay") { [unowned self] (request, response) in
            self.appendToLog("armStay called")
            if let code = self.vistaCode {
                self.sendToVista("\(code)3")
                response.respondWithString("armStay command sent")
            }
            else {
                self.appendToLog("Alarm code has not been set.")
            }
        }

        http?.get("/testMode") { [unowned self] (request, response) in
            self.appendToLog("testMode called")
            if let code = self.vistaCode {
                self.sendToVista("\(code)50")
                response.respondWithString("testMode command sent")
            }
            else {
                self.appendToLog("Alarm code has not been set.")
            }
        }

        http?.get("/causeAlarm") { [unowned self] (request, response) in
            self.appendToLog("causeAlarm called")
            self.sendToVista("\u{2}\u{2}\u{2}")
            response.respondWithString("alarm command sent")
        }
        
        http?.get("/causeFireAlarm") { [unowned self] (request, response) in
            self.appendToLog("causeFireAlarm called")
            self.sendToVista("\u{1}\u{1}\u{1}")
            response.respondWithString("alarm command sent")
        }
        
    }

    //MARK: process Vista messages
    
    func processLine(line: String) {
        
        if showAllVistaMessages {
            appendToLog(lineBuffer)
        }
        
        let nsline = line as NSString
        
        let firstChar = nsline.substringWithRange(NSMakeRange(0,1))
        if firstChar == "[" {
            // this is a normal line
            
            // check if alarm is sounding
            let newAlarmStatus = nsline.substringWithRange(NSMakeRange(11, 1))
            processAlarmStatus(newAlarmStatus, fullText: line)

            // check for chime setting
            let newChimeStatus = nsline.substringWithRange(NSMakeRange(9, 1))
            processChime(newChimeStatus)
            
            // check power status
            let newPowerStatus = nsline.substringWithRange(NSMakeRange(8, 1))
            processPowerStatus(newPowerStatus)
 
            // check if the back up battery is ok
            let newBackupBatteryStatus = nsline.substringWithRange(NSMakeRange(12, 1))
            processBackupBatteryStatus(newBackupBatteryStatus)

            // check arm mode
            let armAwayStatus = nsline.substringWithRange(NSMakeRange(2, 1))
            let armStayStatus = nsline.substringWithRange(NSMakeRange(3, 1))
            processArmMode(armAwayStatus: armAwayStatus, armStayStatus: armStayStatus)
            
            if let range = line.rangeOfString("\"") {
                let start = range.endIndex
                let end = line.endIndex.advancedBy(-2)
                let message = line.substringWithRange(start...end)
                let messageDict = ["message":message]
                //ar error : NSError?
                self.thingie?.getJSONForPOST("message", withData:messageDict, withCallback: "messageResults:")
            }
            
            if let range = line.rangeOfString("\"FAULT ") {
                let start = range.endIndex
                let end = range.endIndex.advancedBy(1)
                let zoneNumber = line.substringWithRange(start...end)
                appendToLog(lineBuffer)
                processFault(zoneNumber)
            }
            else if let range = line.rangeOfString("\"FIRE ") {
                let start = range.endIndex
                let end = range.endIndex.advancedBy(1)
                let zoneNumber = line.substringWithRange(start...end)
                appendToLog(lineBuffer)
                processFault(zoneNumber)
            }
            else if let range = line.rangeOfString("\"CHECK ") {
                let start = range.endIndex
                let end = range.endIndex.advancedBy(1)
                let zoneNumber = line.substringWithRange(start...end)
                appendToLog(lineBuffer)
                processCheck(zoneNumber)
            }
            else if let range = line.rangeOfString("\"LO BAT") {
                let start = range.endIndex
                let end = range.endIndex.advancedBy(1)
                let zoneNumber = line.substringWithRange(start...end)
                appendToLog(lineBuffer)
                processLowBattery(zoneNumber)
            }
            else if let _ = line.rangeOfString("\"Test In Progress") {
                self.armMode = ArmMode.testMode
                self.thingie?.getJSONFor("armMode/test", withCallback: "armModeUpdated:")
                
            }
            else if let _ = line.rangeOfString("Hit * for faults") {
                appendToLog("dealign with hit * for faults")
                sendToVista("*")
            }
            else if let _ = line.rangeOfString("Ready to Arm") {
                processAllClear(line)
            }
            
        }
        
    }

    func processArmMode(armAwayStatus armAwayStatus: NSString, armStayStatus: NSString) {
        if (armAwayStatus == "1") && (self.armMode == nil || self.armMode! != ArmMode.armAway) {
            self.armMode = .armAway
            updateSensor("CTL", attribute: "switch", value: "on")
            appendToLog("armed away")
        }
        else if (armStayStatus == "1") && (self.armMode == nil || self.armMode! != ArmMode.armStay) {
            self.armMode = .armStay
            updateSensor("CTL", attribute: "switch", value: "armStay")
            appendToLog("armed stay")
       }
        else if (armAwayStatus == "0" && armStayStatus == "0") && (self.armMode == nil || self.armMode! != ArmMode.disarmed) {
            self.armMode = .disarmed
            updateSensor("CTL", attribute: "switch", value: "off")
            appendToLog("disarmed")
        }
    }
    
    func processAlarmStatus(newAlarmStatus : NSString, fullText: String) {
        let fire = fullText.rangeOfString("\"FIRE")
        if fire != nil && (self.alarmStatus == nil || self.alarmStatus! != AlarmStatus.fire) {
            alarmStatus = AlarmStatus.fire
            appendToLog("telling ST the alarm is fire")
            updateSensor("ALM", attribute: "alarm", value: "strobe")
        }
        else if (newAlarmStatus == "0") && (self.alarmStatus == nil || self.alarmStatus! != AlarmStatus.off) {
            alarmStatus = AlarmStatus.off
            appendToLog("telling ST the alarm is off")
            updateSensor("ALM", attribute: "alarm", value: "off")
        }
        else if (newAlarmStatus == "1") && (self.alarmStatus == nil || self.alarmStatus! != AlarmStatus.alarm) {
            alarmStatus = AlarmStatus.alarm
            appendToLog("telling ST the alarm is on both")
            updateSensor("ALM", attribute: "alarm", value: "both")
        }
    }
    
    func processChime(newChimeStatus : NSString) {
        if (newChimeStatus == "0") && (self.chimeOn == nil || self.chimeOn! == true) {
            chimeOn = false
            appendToLog("telling ST the chime is off")
            updateSensor("CHM", attribute: "switch", value: "off")
        }
        else if (newChimeStatus == "1") && (self.chimeOn == nil || self.chimeOn! == false) {
            chimeOn = true
            appendToLog("telling ST the chime is on")
            updateSensor("CHM", attribute: "switch", value: "on")
        }
    }

    func processPowerStatus(newStatus : NSString) {
        if (newStatus == "0") && (self.panelOnWallPower == nil || self.panelOnWallPower! == true) {
            panelOnWallPower = false
            appendToLog("telling ST the alarm is on battery power")
            updateSensor("PWR", attribute: "contact", value: "open")
        }
        else if (newStatus == "1") && (self.panelOnWallPower == nil || self.panelOnWallPower! == false) {
            panelOnWallPower = true
            appendToLog("telling ST the alarm is on wall power")
            updateSensor("PWR", attribute: "contact", value: "closed")
        }
    }

    func processBackupBatteryStatus(newStatus : NSString) {
        if newStatus == "0" && self.panelBatteryStatus != SensorStatus.ok {
            panelBatteryStatus = SensorStatus.ok
            appendToLog("telling ST the alarm battery is ok")
            updateSensor("CTL", attribute: "status", value: "ok")
        }
        else if newStatus == "1" && self.panelBatteryStatus != SensorStatus.lowBattery {
            panelBatteryStatus = SensorStatus.lowBattery
            appendToLog("telling ST the alarm battery is low")
            updateSensor("CTL", attribute: "status", value: "lowbattery")
        }
    }
    
    func processCheck(zoneNumber: String) {
        if let _ = self.zoneDict[zoneNumber] {   // is it for a zone we know about?
            if let oldState = self.zoneState[zoneNumber] {    // do we know the last state of the sensor?
                if oldState.sensorStatus != SensorStatus.check {
                    updateSensor(zoneNumber, attribute: "status", value: "check")
                    self.zoneState[zoneNumber]?.sensorStatus = SensorStatus.check
                    self.zoneState[zoneNumber]?.lastUpdateTime = NSDate().timeIntervalSince1970
                }
            }
        }
        checkIfOtherZonesHaveCleared()
    }
    
    func processLowBattery(zoneNumber: String) {
        if let zoneInfo = self.zoneDict[zoneNumber] {   // is it for a zone we know about?
            if let _ = self.sensorAttributes[zoneInfo.type] {  // do we know the senosr type attributes?
                if let oldState = self.zoneState[zoneNumber] {    // do we know the last state of the sensor?
                    if oldState.sensorStatus != SensorStatus.lowBattery {
                        updateSensor(zoneNumber, attribute: "status", value: "lowBattery")
                        self.zoneState[zoneNumber]?.sensorStatus = SensorStatus.lowBattery
                        self.zoneState[zoneNumber]?.lastUpdateTime = NSDate().timeIntervalSince1970
                    }
                }
            }
        }
        checkIfOtherZonesHaveCleared()
    }
    
    func processFault(zoneNumber: String) {
        if let zoneInfo = self.zoneDict[zoneNumber] {   // is it for a zone we know about?
            if let sensorAttributes = self.sensorAttributes[zoneInfo.type] {  // do we know the senosr type attributes?
                if let oldState = self.zoneState[zoneNumber] {    // do we know the last state of the sensor?
                    if oldState.attributeState != sensorAttributes.faultedAttributeValue {
                        updateSensor(zoneNumber, attribute: sensorAttributes.attributeName, value: sensorAttributes.faultedAttributeValue)
                        self.zoneState[zoneNumber]?.attributeState = sensorAttributes.faultedAttributeValue
                        self.zoneState[zoneNumber]?.lastUpdateTime = NSDate().timeIntervalSince1970
                    }
                    else {
                        self.zoneState[zoneNumber]?.lastUpdateTime = NSDate().timeIntervalSince1970
                    }
                }
            }
        }
        checkIfOtherZonesHaveCleared()
    }
    
    func processAllClear(line: String) {
        for (zoneNumber, zoneInfo) in self.zoneDict {
            if zoneInfo.getsFaultMessages {
                let sensorAttributes = self.sensorAttributes[zoneInfo.type]!
                if self.zoneState[zoneNumber]?.attributeState != sensorAttributes.clearAttributeValue {
                    updateSensor(zoneNumber, attribute: sensorAttributes.attributeName, value: sensorAttributes.clearAttributeValue)
                    self.zoneState[zoneNumber]?.attributeState = sensorAttributes.clearAttributeValue
                    self.zoneState[zoneNumber]?.lastUpdateTime = NSDate().timeIntervalSince1970
                }
                else {
                    self.zoneState[zoneNumber]?.lastUpdateTime = NSDate().timeIntervalSince1970
                }
                if self.zoneState[zoneNumber]?.sensorStatus != SensorStatus.ok {
                    // not getting check or low bat messages for the zone
                    updateSensor(zoneNumber, attribute: "status", value: "ok")
                    self.zoneState[zoneNumber]?.sensorStatus = SensorStatus.ok
                }
            }

        }
        // check to see if the backup battery is now ok
        if self.panelBatteryStatus != SensorStatus.ok {
            self.panelBatteryStatus = SensorStatus.ok
            updateSensor("CTL", attribute: "status", value: "OK")
        }
    }
    
    func checkIfOtherZonesHaveCleared() {
        let now = NSDate().timeIntervalSince1970
        for (zoneNumber, currentState) in self.zoneState {
            let zoneInfo = zoneDict[zoneNumber]!          // this must exist or we are screwed
            if let attributes = sensorAttributes[zoneInfo.type] {
                if currentState.attributeState == attributes.faultedAttributeValue && now - currentState.lastUpdateTime > 5.0 {
                    // this zone stopped faulting though other zones still are
                    if let sensorAttributes = self.sensorAttributes[zoneInfo.type] {
                        if !showAllVistaMessages {
                            appendToLog("zone \(zoneNumber) now: \(now) last: \(currentState.lastUpdateTime), diff: \(now - currentState.lastUpdateTime)")
                        }
                        updateSensor(zoneNumber, attribute: sensorAttributes.attributeName, value: sensorAttributes.clearAttributeValue)
                        self.zoneState[zoneNumber]?.attributeState = sensorAttributes.clearAttributeValue
                    }
                }
                if currentState.sensorStatus != SensorStatus.ok && now - currentState.lastUpdateTime > 5.0 {
                    // not getting check or low bat messages for the zone
                    updateSensor(zoneNumber, attribute: "status", value: "ok")
                    self.zoneState[zoneNumber]?.sensorStatus = SensorStatus.ok
                }            }
        }
    }

    //MARK: AKSmartThingsDeletate
    
    func handleAccessToken(accessToken: String!) {
        appendToLog("access token is \(accessToken)")
        PreferenceData.standardDefaults().smartAppAccessToken = accessToken
        self.accessToken = accessToken
    }
    
    func handleError(error: NSError!) {
        appendToLog("error was \(error.description)")
    }
    
    func readyForApiRequests(sender: AKSmartThings!) {
        appendToLog("ready for api requests")
        let endpoints = self.thingie?.endpoints
        appendToLog("endpoints are \(endpoints)")
        self.thingie?.getJSONFor("clientIP/\(self.ipAddress):\(self.http!.port())", withCallback: "clientIPCallback:")
    }
   
    //MARK: ORSSerialPort sending
    func sendToVista(cmd: String) {
        let data = (cmd as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        self.serialPort?.sendData(data)
    }
    
    //MARK: ORSSerialPortDelegate
    
    func serialPortWasRemovedFromSystem(serialPort: ORSSerialPort!) {
        self.serialPort = nil
    }
    
    func serialPort(serialPort: ORSSerialPort!, didReceiveData data: NSData!) {
        if let s = NSString(data: data, encoding: NSUTF8StringEncoding) {
            let buffer: String = s as String
            for c in buffer.characters {
                 if c == "\r\n" {
                    processLine(lineBuffer)
                    lineBuffer = ""
                }
                else {
                    lineBuffer.append(c)
                }
            }
        }
    }
    
}

