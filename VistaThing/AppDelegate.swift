//
//  AppDelegate.swift
//  VistaThing
//
//  Created by Linda Thomas on 10/24/14.
//  Copyright (c) 2014 Linda Thomas. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {


    func applicationDidFinishLaunching(aNotification: NSNotification) {
        
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        NSUserDefaults.standardUserDefaults().synchronize()
    }

}

