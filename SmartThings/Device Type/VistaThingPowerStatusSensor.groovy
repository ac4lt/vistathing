/**
 *  VistaThing Virtual Power Status Sensor
 *
 *  VistaThing Virtual Contact Sensor
 *
 *  Copyright 2014 Linda Thomas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 *  This implements a standard SmartThings contact sensor that is used only for the Vista alarm panel power status.
 *  If the alarm panel is running on AC power it reports "closed". If the alarm panel is running it reports "open".
 *  
 *  This can be used as a good indication of a power failure in your house. You could use the "notify me when"
 *  smartapp to tell you that a power failure has happened at your home when this sensor is "open".
 *  
 *  From a UI perspective this tile doesn't dsplay "open" or "closed" but shows "AC" or "Battery".
 *
 */
metadata {
	// Automatically generated. Make future change here.
	definition (name: "VistaThing Power Status Sensor", namespace: "ac4lt", author: "Linda Thomas") {
		capability "Contact Sensor"
        capability "Refresh"
    }

	simulator {
	}
    
	tiles {
		standardTile("contact", "device.contact", width: 2, height: 2) {
			state("open", label:'Battery', icon:"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRaYZNkDuC9tafpN4wXAXg7OmWFgafS5nu-nRNSQGhWZjLBY_69",  backgroundColor:"#ffa81e")
			state("closed", label:'AC', icon:"st.switches.switch.on", backgroundColor:"#79b821")
		}
        standardTile("refresh", "device.refresh", inactiveLabel: false, decoration: "flat") {
			state "default", action:"refresh.refresh", icon:"st.secondary.refresh"
		}
		main(["contact"])
		details(["contact", "refresh"])
	}
}

def parse(Map map) {
	log.debug "in map parse with $map"
    def attribute = map.name
    map
}

def parse(String description) {
	def results
	log.debug "in string parse with ${description}"
	return results
}

def refresh() {
	log.trace "refresh called"
    def remoteAddress = parent.state.remoteVista
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/refresh/${state.zone}",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def setZone(zone) {
	state.zone = zone
}

def getZone() {
  state.zone
}