/**
 *  VistaThing Alarm
 *
 *  Copyright 2014 Linda Thomas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 *  This implements a standard SmartThings alarm device. It corresponds the alarm sounder on the Vista system.
 *
 *  The large 2x2 tile reflects the current alarm state while the "siren" and "fire" buttons wil cause the alarm to sound.
 *  The "off" button silences the alarm (it also disarms the system since that is how alarms are silenced on the Vista).
 *
 *  SmartThings has four alarm states. They are mapped to the Vista alarm states as follows:
 *      SmartThings state     Vista state
 *      off                   off
 *      siren                 alarm
 *      strobe                fire
 *      both                  alarm
 *
 *  Since the Vista can't do both states, we map "both" to an alarm.
 *
 */
metadata {
	definition (name: "VistaThing Alarm", namespace: "ac4lt", author: "Linda Thomas") {
		capability "Alarm"
        capability "Refresh"
        // attributes for push-only buttons
        attribute "offButton", "string"
        attribute "sirenButton", "string"
        attribute "strobeButton", "string"
 	}

	simulator {
		// TODO: define status and reply messages here
	}

	tiles {
		standardTile("alarm", "device.alarm", width: 2, height: 2,) {
			state "off", label:'off', icon:"st.security.alarm.clear", backgroundColor:"#79b821" 
			state "siren", label:'Alarm!',icon:"st.security.alarm.alarm", backgroundColor:"#E81321"
			state "strobe", label:'Fire!', icon:"st.security.alarm.partial", backgroundColor:"#E81321"
			state "both", label:'Trouble!', icon:"st.security.alarm.alarm", backgroundColor:"#E81321"
			state "default", label:'Unknown',icon:"st.secondary.secondary", backgroundColor:"#E81321"
		}
		standardTile("sirenButton", "device.sirenButton") {
			state "default", label:'Alarm!', action:"alarm.siren", icon:"st.security.alarm.alarm", backgroundColor:"#E81321"
		}
		standardTile("strobeButton", "device.strobeButton") {
			state "default", label:'Fire!', action:"alarm.strobe", icon:"st.security.alarm.alarm", backgroundColor:"#E81321"
		}
		standardTile("offButton", "device.offButton") {
			state "enabled", label:'off', action:"alarm.off", icon:"st.security.alarm.clear", backgroundColor:"#79b821"
		}
        standardTile("refresh", "device.refresh", inactiveLabel: false, decoration: "flat") {
			state "default", action:"refresh.refresh", icon:"st.secondary.refresh"
		}
		main "alarm"
		details(["alarm","sirenButton","strobeButton","offButton", "refresh"])
    }
}

// parse events into attributes
def parse(String description) {
	log.debug "Parsing '${description}'"
}

def parse(Map map) {
	//log.debug "map[name] is ${map["name"]}"
	map
}


// handle commands

def off() {
	log.debug "Executing off"
    def remoteAddress = parent.state.remoteVista // device.currentValue("remoteVistaThingAddress")
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/disarm",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def on() {
	log.trace "executing on"
    def remoteAddress = parent.state.remoteVista // device.currentValue("remoteVistaThingAddress")
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/causeAlarm",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def siren() {
	log.trace "executing siren"
    on()
}

def strobe() {
	log.debug "Executing strobe"
    def remoteAddress = parent.state.remoteVista // device.currentValue("remoteVistaThingAddress")
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/causeFireAlarm",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def both() {
	log.debug "Executing both"
    // vista can't do both so we'll pick siren by arbitrary choice
    on()
}

def refresh() {
	log.trace "refresh called"
    def remoteAddress = parent.state.remoteVista
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/refresh/${state.zone}",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def setZone(zone) {
	state.zone = zone
}

def getZone() {
  state.zone
}