/**
 *  VistaThing Virtual Contact Sensor
 *
 *  Copyright 2014 Linda Thomas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 *  This implements a standard SmartThings contact sensor that can be used for Vista door, window and glass break
 *  sensors. The sensor registers "closed" when the sensor is in the clear state and "open" when in the faulted
 *  state.
 *
 *  In addition te large 2x2 tile that reports the device status there is also another tile that reports
 *  on a "status" attribute that is specific to VistaThing. If the Vista panel reports the sensor is in a low battery
 *  or "check" state that will be reported in the status tile.
 * 
 */
metadata {
	// Automatically generated. Make future change here.
	definition (name: "VistaThing Contact Sensor", namespace: "ac4lt", author: "Linda Thomas") {
		capability "Contact Sensor"
        capability "Refresh"
        attribute "status", "string"                    // ok, lowbattery, check
    }

	simulator {
	}
    
	tiles {
		standardTile("contact", "device.contact", width: 2, height: 2) {
			state("open", label:'${name}', icon:"st.contact.contact.open", backgroundColor:"#ffa81e")
			state("closed", label:'${name}', icon:"st.contact.contact.closed", backgroundColor:"#79b821")
		}
		valueTile("status", "device.status") {
			state "default", label:'${currentValue}', backgroundColor:"#ffffff"
		}		
        standardTile("refresh", "device.refresh", inactiveLabel: false, decoration: "flat") {
			state "default", action:"refresh.refresh", icon:"st.secondary.refresh"
		}
		main(["contact"])
		details(["contact", "status", "refresh"])
	}
}

def parse(Map map) {
	log.debug "in map parse with $map"
    def attribute = map.name
    map
}

def parse(String description) {
	def results
	log.debug "in string parse with ${description}"
	return results
}

def refresh() {
	log.trace "refresh called"
    def remoteAddress = parent.state.remoteVista
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/refresh/${state.zone}",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def setZone(zone) {
	state.zone = zone
}

def getZone() {
  state.zone
}