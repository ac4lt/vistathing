/**
*  VistaThing Alarm Controller
*
*  Copyright 2014-2015 Linda Thomas-Fowler
*
*  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
*  in compliance with the License. You may obtain a copy of the License at:
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
*  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
*  for the specific language governing permissions and limitations under the License.
*
*  This implements the alarm controller device. It acts as a standard SmartThings switch. Setting the switch 'on'
*  puts it in armAway mmode where 'off' disarms the system. However, it also supports a nonstandard switch setting
*  of 'armStay' that puts the alarm system into 'armStay' mode. An additional switch value of 'testMode' puts the
*  alarm into testing mode.
*
*  In the ST mobile app, the large 2x2 tile reflects the current arming state. There are four button tiles that
*  will instruct the alarm to disarm, arm away, arm stay or go into test mode.
*
*  The status tile reflects the condition of the backup battery.
*
*  The panelMessage displays the latest message from the alarm panel and should be a duplicate of what
*  is currently displaying on your actual alarm key pad.
*
*/
metadata {
definition (name: "VistaThing Alarm Controller", namespace: "ac4lt", author: "Linda Thomas-Fowler") {
capability "Switch"
capability "Battery"
capability "Refresh"


command "disarm"
command "armAway"
command "armStay"
command "testMode"

//attribute "remoteVistaThingAddress", "string" // e.g. 192.168.1.16:9876
attribute "panelMessage", "string"              // messages from the alarm panel
attribute "status", "string"                    // ok, lowbattery
// attributes for push-only buttons
attribute "disarmButton", "string"
attribute "armAwayButton", "string"
attribute "armStayButton", "string"
attribute "testButton", "string"
}

simulator {
// TODO: define status and reply messages here
}

tiles {
standardTile("switch", "device.switch", width: 2, height: 2) {
state "on", label:'Armed Away', icon:"st.security.alarm.on", backgroundColor:"#ffa81e"
state "off", label:'Disarmed',icon:"st.security.alarm.off", backgroundColor:"#79b821"
state "armStay", label:'Armed Stay', icon:"st.security.alarm.partial", backgroundColor:"#ffa81e"
state "test", label:'Test Mode', icon:"st.Kids.kids6", backgroundColor:"#8ADEFF"
state "default", label:'Unknown',icon:"st.secondary.secondary", backgroundColor:"#ffffff"
}
standardTile("disarmButton", "device.disarmButton") {
state "enabled", label:'Disarm', action:"disarm", icon:"st.security.alarm.off", backgroundColor:"#79b821"
}
standardTile("armAwayButton", "device.armAwayButton") {
state "enabled", label:'Away', action:"armAway", icon:"st.security.alarm.on", backgroundColor:"#ffa81e"
}
standardTile("armStayButton", "device.armStayButton") {
state "eanbled", label:'Stay', action:"armStay", icon:"st.security.alarm.partial", backgroundColor:"#ffa81e"
}
standardTile("testButton", "device.testButton") {
state "enabled", label:'Test', action:"testMode", icon:"st.Kids.kids6", backgroundColor:"#8ADEFF"
}
valueTile("status", "device.status") {
state "default", label:'${currentValue}', backgroundColor:"#ffffff"
//state "ok", label:'OK',backgroundColor:"#79b821"
//state "lowBattery", label:'Low Battery',backgroundColor:"#ffa81e"
//state "default", label:'Unknown',backgroundColor:"#333333"
}
valueTile("panelMessage", "device.panelMessage", width: 3, height:1, decoration: "flat") {
state "default", label:'${currentValue}'
}
standardTile("refresh", "device.refresh", inactiveLabel: false, decoration: "flat") {
state "default", action:"refresh.refresh", icon:"st.secondary.refresh"
}

main(["switch"])
details(["switch", "disarmButton", "armAwayButton", "status", "testButton", "armStayButton", "panelMessage", "refresh"])
}

}

// parse events into attributes
def parse(String description) {
log.debug "Parsing '${description}'"
}

def parse(Map map) {
//log.debug "map is $map"
if (map["name"] != "panelMessage") {
log.debug "in map parse with $map"
}
map
}


// handle commands

def off() {
disarm()
}

def on() {
armAway()
}

def disarm() {
log.debug "disarm"
def remoteAddress = parent.state.remoteVista //device.currentValue("remoteVistaThingAddress")
log.debug "remote address is $remoteAddress"
def action = new physicalgraph.device.HubAction([
method: "GET",
path: "/disarm",
headers: [
HOST: "$remoteAddress"
]
])
action
}

def armAway() {
log.debug "Executing 'arm away'"
def remoteAddress = parent.state.remoteVista //device.currentValue("remoteVistaThingAddress")
log.debug "remote address is $remoteAddress"
def action = new physicalgraph.device.HubAction([
method: "GET",
path: "/armAway",
headers: [
HOST: "$remoteAddress"
]
])
action
}

def armStay() {
log.debug "Executing 'arm stay'"
def remoteAddress = parent.state.remoteVista //device.currentValue("remoteVistaThingAddress")
log.debug "remote address is $remoteAddress"
def action = new physicalgraph.device.HubAction([
method: "GET",
path: "/armStay",
headers: [
HOST: "$remoteAddress"
]
])
action
}

def testMode() {
log.debug "Executing 'test mode'"
def remoteAddress = parent.state.remoteVista //device.currentValue("remoteVistaThingAddress")
log.debug "remote address is $remoteAddress"
def action = new physicalgraph.device.HubAction([
method: "GET",
path: "/testMode",
headers: [
HOST: "$remoteAddress"
]
])
action
}


def refresh() {
log.trace "refresh called"
def remoteAddress = parent.state.remoteVista
log.debug "remote address is $remoteAddress"
def action = new physicalgraph.device.HubAction([
method: "GET",
path: "/refresh/${state.zone}",
headers: [
HOST: "$remoteAddress"
]
])
action
}

def setZone(zone) {
state.zone = zone
}

def getZone() {
state.zone
}

