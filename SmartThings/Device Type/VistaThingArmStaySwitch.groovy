/**
 *  VistaThing Arm Stay Switch
 *
 *  Copyright 2014 Linda Thomas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 *  Even though SmartThings can handle custom attributes it doesn't really know what to do with them. In order to
 *  use them with most other smart apps, attributes need to be exposed as a standard switch. Although the Alarm
 *  Controller device does this, normal smart apps can only set it to on (arm away) or off. This device fills the
 *  gap for smart apps that need to set the alarm controller to arm stay. Setting this switch to on
 *  will attempt to set the alarm controller to arm stay. 
 *
 *  In the ST mobile app, the large 2x2 tile reflects the current arming state. *
 */
metadata {
	definition (name: "VistaThing Arm Stay Switch", namespace: "ac4lt", author: "Linda Thomas") {
		capability "Switch"
        capability "Refresh"
    }

	simulator {
		// TODO: define status and reply messages here
	}

	tiles {
		standardTile("switch", "device.switch", width: 2, height: 2) {
			state "on", label:'Armed Stay', icon:"st.security.alarm.on", backgroundColor:"#ffa81e", action:"switch.of"
			state "off", label:'Disarmed',icon:"st.security.alarm.off", backgroundColor:"#79b821", action: "switch.on"
		}
	    standardTile("refresh", "device.refresh", inactiveLabel: false, decoration: "flat") {
			state "default", action:"refresh.refresh", icon:"st.secondary.refresh"
		}
        main(["switch"])
		details(["switch", "refresh"])
	}
    
}
    
// parse events into attributes
def parse(String description) {
	log.debug "Parsing '${description}'"
}

def parse(Map map) {
	//log.debug "map is $map"
	if (map["name"] != "panelMessage") {
		log.debug "in map parse with $map"
    }
    map
}


// handle commands

def off() {
	disarm()
}

def on() {
	armStay()
}

def disarm() {
	log.debug "disarm"
    def remoteAddress = parent.state.remoteVista //device.currentValue("remoteVistaThingAddress")
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/disarm",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def armStay() {
	log.debug "Executing 'arm stay'"
    def remoteAddress = parent.state.remoteVista //device.currentValue("remoteVistaThingAddress")
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/armStay",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def refresh() {
	log.trace "refresh called"
    def remoteAddress = parent.state.remoteVista
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/refresh/${state.zone}",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def setZone(zone) {
	state.zone = zone
}

def getZone() {
  state.zone
}

