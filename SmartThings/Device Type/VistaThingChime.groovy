/**
 *  VistaThing Chime
 *
 *  Copyright 2014 Linda Thomas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 *  The VistaThing Chime implements a standard SmartThings switch that turns the Vista's chime on or off.
 *  When "on" the Vista chime will beep and possibly speak the names of the zones as fualts happen. When off
 *  the Vista will not announce faults.
 *
 */
metadata {
	definition (name: "VistaThing Chime", namespace: "ac4lt", author: "Linda Thomas") {
		capability "Switch"
        capability "Refresh"
    }

	simulator {
		// TODO: define status and reply messages here
	}

	tiles {
		standardTile("switch", "device.switch", width: 2, height: 2) {
			state "on", label:'${currentValue}', action:"switch.off", icon:"st.custom.sonos.unmuted", backgroundColor:"#8ADEFF", nextState:"default"
			state "off", label:'${currentValue}', action:"switch.on", icon:"st.custom.sonos.muted", backgroundColor:"#ffffff", nextState:"default"
			state "default", label:'Updating', icon:"st.custom.sonos.muted", backgroundColor:"#ffffff"
		}
        standardTile("refresh", "device.refresh", inactiveLabel: false, decoration: "flat") {
			state "default", action:"refresh.refresh", icon:"st.secondary.refresh"
		}

        main(["switch"])
		details(["switch", "refresh"])
	}
}

// parse events into attributes
def parse(String description) {
	log.debug "Parsing '${description}'"
}

def parse(Map map) {
	//log.debug "map[name] is ${map["name"]}"
    map
}


// handle commands

def off() {
	log.debug "Executing off"
	toggleChime()
}

def on() {
	log.debug "Executing on"
    toggleChime()
}

def toggleChime() {
	log.debug "Executing toggle chime"
    def remoteAddress = parent.state.remoteVista // device.currentValue("remoteVistaThingAddress")
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/toggleChime",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def refresh() {
	log.trace "refresh called"
    def remoteAddress = parent.state.remoteVista
    log.debug "remote address is $remoteAddress"
	def action = new physicalgraph.device.HubAction([
		method: "GET",
		path: "/refresh/${state.zone}",
		headers: [
			HOST: "$remoteAddress"
		]
    ])
    action
}

def setZone(zone) {
	state.zone = zone
}

def getZone() {
  state.zone
}