/**
*  VistaThing
*
*  Copyright 2014 Linda Thomas
*
*  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
*  in compliance with the License. You may obtain a copy of the License at:
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
*  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
*  for the specific language governing permissions and limitations under the License.
*
*
*  VistaThing is the SmartThings side of a SmartThings/Mac combination to talk to Vista Alarm panels.
*  For details on setting this up see the readme in the project archive.
*
*/

definition(
name: "VistaThing",
namespace: "ac4lt",
author: "Linda Thomas-Fowler",
description: "Designed to work with the VistaThing Mac app to integrate a Vista alarm panel into SmartThings",
category: "Safety & Security",
iconUrl: "http://ac4lt.org/icons/VistaThing-256.png",
iconX2Url: "http://ac4lt.org/icons/VistaThing-256@2x.png",
oauth: true)


preferences {
section("Using the selected hub, VistaThing will create virtual SmartThings devices to correspond with each Vista sensor you have. These new devices will appear in SmartThings and can be used like any other SmartThings device.") {
input "theHub", "hub", multiple: false, required: true
}
section( "Notify if the alarm is tripped" ) {
input "sendPushMessage", "enum", title: "Send a push notification?", options: ["Yes","No"], required:false
input "phone", "phone", title: "Send a Text Message?", required: false
}
}

mappings {
path("/sensors") {
action: [
GET: "listSensors"
]
}
path("/createSensor") {
action: [
POST: "createSensor"
]
}
path("/deleteSensor") {
action: [
POST: "deleteSensor"
]
}
path("/sensor/:zone/:attribute/:newState") {
action: [
GET: "updateSensor"
]
}
path("/chime/:newState") {
action: [
GET: "updateChime"
]
}
path("/armMode/:newState") {
action: [
GET: "updateArmMode"
]
}
path("/clientIP/:ip") {
action: [
GET: "updateRemoteIP"
]
}
path("/message") {
action: [
POST: "updateMessage"
]
}
}

def installed() {
log.debug "Installed with settings: ${settings}"
initialize()
}

def updated() {
log.debug "Updated with settings: ${settings}"

unsubscribe()
initialize()
}

def initialize() {
state.controllerZone = "CTL"
state.controllerNetworkId = "VistaThing ${state.controllerZone}"
if (state.sensorName == null) {
state.sensorName = [:]
}
}

// used by remote end to tell us what sensors we have
def createSensor() {

log.trace "in createSensor"
def body = request.JSON
log.debug "json is ${body}"

def zone       = body["zone"]
def sensorType = body["type"]
def sensorName = body["name"]

log.debug "zone is *$zone*"
log.debug "name is *$sensorName*"
log.debug "type is *$sensorType*"

def networkID = "VistaThing " + zone

def sensor = [zone: zone, name: sensorName, type: sensorType, networkID: networkID]
def sensorCreated = false
def errorMsg = ""

//try {
def d = addChildDevice("ac4lt", sensorType, networkID, theHub.id,
["name":sensorName, "label":sensorName, "completedSetup":true])
d.setZone(zone)
state.sensorName[zone] = sensorName
sensorCreated = true
log.debug "created sensor $sensorName, zone $zone, type: $sensorType"
//}
//catch (Throwable e) {
//   	log.error "Unable to create device $sensor for reason $e"
//    errorMsg = "${e.getCause()} -- ${e.getMessage()}"
//}


if (sensorCreated) {
sensor
}
else {
[error: "sensor not created", reason: errorMsg]
}

}

def deleteSensor() {
log.debug "in deleteSensor"
def body = request.JSON
log.debug "body is $body"
def id = body["id"]
log.debug "id is $id"
def msg = ""
//try {
deleteChildDevice(id)
state.sensorName.remove(zone)
msg = ["message": "Sensor $id has been deleted."]
//}
//catch (Throwable e) {
//  log.error "Unable to delete device $sensor for reason $e"
//msg = ["error": "${e.getCause()} -- ${e.getMessage()}"]
// }
msg
}

def listSensors() {
log.trace "in listSensors"
def sensors = [:]
def devs = getChildDevices()
devs.each {
def name = it.displayName
def zone = it.getZone()
def id = it.deviceNetworkId
def type = it.typeName
def sensor = [zone: zone, name: name, type: type, networkID: id]
sensors[zone] = sensor
log.debug "device is *${name}*, zone: *${zone}*, id: *${id}*, type: *${type}"
}
log.debug "child devices: $devs"
sensors
}

def updateSensor() {
log.trace "in updateSensor"
log.debug "zone is ${params.zone}, attribute is ${params.attribute}, new state is ${params.newState}"
def zone = params.zone
def attribute = params.attribute
def newState = params.newState
def networkID = getNetworkId(zone)
def device = getChildDevice(networkID)
def sensorName = device.displayName  //state.sensorName[zone]
def description = nil
if (attribute == "motion" && newState == "active") {
description = "${sensorName} Detected Motion"
}
else if (attribute == "motion" && newState == "inactive") {
description = "${sensorName} Motion Has Stopped"
}
else if (attribute == "contact" && newState == "open") {
description = "${sensorName} Was Opened"
}
else if (attribute == "contact" && newState == "closed") {
description = "${sensorName} Was Closed"
}
else {
description = "${sensorName} ${attribute} is ${newState}"
}
def returnValue = null
//log.debug "sending event to *${networkID}*"
try {
def properties = [name:attribute, value:newState, linkText: sensorName, descriptionText: description]
log.debug("properties are ${properties}")
sendEvent(device, properties)
returnValue = ["zone": zone, "attribute":attribute, "new state":newState]
}
catch (Throwable e) {
def msg = "${e.getCause()} -- ${e.getMessage()}"
returnValue = ["zone":zone, "error": msg]
}
if (zone == "ALM" && newState != "off") {
send("Alarm is sounding, (alarm type is ${newState}")
}
returnValue
}

def updateArmMode() {
log.trace "in updateArmMode"
log.debug " new state is ${params.newState}"
def attribute = "switch"
def newState = params.newState
def returnValue = null
def device = getChildDevice(state.controllerNetworkId)
def sensorName = state.sensorName[zone]
log.debug "modifying arm mode on *${state.controllerNetworkId}*"
try {
def properties =  [name:attribute, value:newState, linkText: sensorName]
log.debug("properties are ${properties}")
sendEvent(device, properties)
returnValue = ["message": "arm mode set ${newState}"]
}
catch (Throwable e) {
def msg = "${e.getCause()} -- ${e.getMessage()}"
returnValue = ["error":"chime", "message": msg]
}
returnValue
}


// used by remote end to tell us it's ip address so we can initiate calls
def updateRemoteIP() {

log.trace "in updateRemoteIP"
def ip = params.ip
log.debug "ip is ${ip}"
def returnValue = null
state.remoteVista = ip
["message": "remote ip updated to $ip"]
}

// used by remote end to tell us the current alarm panel message
def updateMessage() {

//log.trace "in updateMessage"
def body = request.JSON
//log.debug "json is ${body}"

def message = body["message"]
//log.debug "message is $message"

def device = getChildDevice(state.controllerNetworkId)
def sensorName = state.sensorName[zone]

def returnValue = null
//log.debug "sending event to *${state.controllerNetworkId}*"
try {
sendEvent(state.controllerNetworkId, [name:"panelMessage", value:message, displayed:false])
returnValue = ["message": "message updated"]
}
catch (Throwable e) {
def msg = "${e.getCause()} -- ${e.getMessage()}"
returnValue = ["error":"message update", "message": msg]
}
returnValue
}

// generate network id from zone
def getNetworkId(zone) {
"VistaThing $zone"
}

private send(msg) {
if ( sendPushMessage != "No" ) {
log.debug( "sending push message" )
sendPush( msg )
}

if ( phone ) {
log.debug( "sending text message" )
sendSms( phone, msg )
}

log.debug msg
}