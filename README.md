# README #

This is a project to integrate Vista alarm panels with SmartThings using a NuTech AD2USB interface and a Mac computer.

### Version History ###

* 2015-09-23 - 1.5 - The mac app was sending the wrong id for the chime over to ST. Fixed.
* 2015-09-22  (no tag yet) - ST alarm controller device type now as battery capability though it doesn't actually report battery status at this point in time. This was done to allow the battery watchdog smart app monitor the alarm controller as it periodically sends alarm panel status update messages. If these messages aren't received then it's a good indication that things have gone south.
* 2015-09-21 - 1.4 - VistaThing smartapp now generates SmartThing events that look more like native events in the SmartThings activity feed.
* 2015-09-16 - (no tag yet) - updated for swift 2; now requires xcode 7
* 2014-11-24 - 1.3 - can now go directly from arm mode to arm mode
* 2014-11-24 - (no tag yet) - fixed date formatting in mac VistaThing log
* 2014-11-24 - (no tag yet) - fixed reversed actions on arm away and arm stay switch device types
* 2014-11-20 - (no tag yet) - VistaThing smart app now offers to send messages when alarm goes off
* 2014-11-20 - (no tag yet) - 2 new convenience device types (Arm Away Switch and Arm Stay Switch) to easily put the alarm into the desired mode
* 2014-11-18 - 1.2 - even newer icon for VistaThing smartapp and icon for mac app; fixed typo in mac logging
* 2014-11-18 - 1.1 - new icon for VistaThing smartapp; reworked mac app preferences a bit.
* 2014-11-15 - 1.0 - Initial release

### Acknowledgements ###

Before getting into the details, I would like to acknowledge those whose efforts made this possible. Without their prior art, this project would have taken much longer and may not have happened at all.

On the Mac side, I used code from the following projects:

* CocoaHTTPServer - https://github.com/robbiehanson/CocoaHTTPServer
* RoutingHTTPServer - https://github.com/mattstevens/RoutingHTTPServer
* AKSmartThings - https://github.com/alexking/AKSmartThings

There are also dependencies of those projects that are included:

* CocoaAsyncSocket
* CocoaLumberjack

On the SmartThings side, there were several people who helped either by having posted source code to similar projects or by answering SmartThings questions on the SmartThings community forum. These include by are not limited to:

* Craig Lyons (craig) - author of https://gist.github.com/lyons189/e5b30109fdaec805d474
* Geko
* tslagle13
* and probably others who have escaped the recall of my brain cell.

### Requirements ###

* A Vista alarm panel (not sure which versions will work, I have a 20p)
* A NuTech AD2USB
* Mac running OS X 10.10
* Xcode 7 or higher

### How do I get set up? ###

* Get an [AD2USB][http://www.alarmdecoder.com/catalog/product_info.php/cPath/1/products_id/29] from alarm decoder.com. You will also need a length of 4-conductor cable to wire the device to your alarm panel and a mini-USB cable to connect to your mac.
* Wire up the AD2USB as per http://www.alarmdecoder.com/wiki/index.php/Installation_Videos
* According to NuTech the appropriate driver is already installed on the mac starting with OS X 10.9 but this appears to be incorrect. I had to go to http://www.ftdichip.com/Drivers/VCP.htm to get the VCP driver for Mac.
* You can test the communication with the AD2USB using the mac screen command from Terminal. Determine the pathname of the serial device as described in the videos referenced above. You'll need this info later. It will probably be something that looks like /dev/tty.usbserial-DJ005UX4 where the "DJ005UX4" portion will be the serial number from your own device.
* You may need to configure the keypad address of the AD2USB. Using screen, you can do a C command and it will walk you through the process. We had three keypads at our house at address 16, 17 and 18 so I had to change the AD2USB to 19. I also had to add device 19 to the alarm panel programming through the alarm keypad. I'll see if I can get detailed instructions for that.
* Once you have confirmed that you can see alarm panel messages flow by with screen you are ready to continue.
* Log into SmartThings and go to the developer IDE.
* Go to 'My Device Types' and add in each of the 8 device type files (each one is a separate device).
    * VistaThingAlarm.groovy
    * VistaThingAlarmController.groovy
    * VistaThingArmAwaySwitch.groovy
    * VistaThingArmStaySwitch.groovy
    * VistaThingChime.groovy
    * VistaThingContactSensor.groovy
    * VistaThingMotionSensor.groovy
    * VistaThingPowerSensor.groovy
    * VistaThingSmokeDetector.groovy
* Then go to "My SmartApps" and add
    * VistaThing.groovy
    * After adding this sone, be sure to enable OAuth for its settings. Copy the values for the oath client id and the oath client secret. You need these for the mac.
* After you save each one be sure to also choose "publish for me".
* That should do it for the SmartThings side. Now we need to config the mac app.
* Open Xcode and open the VistaThing project
* Edit VistaThingController.swift and edit the definedZones data structure to match your Vista configuration. The ALM, CTL and CHM zones are fake zones that correspond to the Alarm Controller, Alarm and Chime devices. Don't delete or edit them. Only edit the normal zones.
* Build and run VistaThing
* Open VistaThings preferences. Paste in the values for the client id and client secret from above. Also enter the pathname for the AD2USB serial port and the code to for your alarm. Leave the access token blank. That will get filled in for you automatically.
* Click the "connect to SmartThings" button. That will initiate the oauth authentication process. Fill in your ST credentials and then select the hub you want to connect with VistaThings and you should be connected. 
* Click the "Create Virtual Devices" button. This should send over your zone information. Look for the completion message when it's done. When it is done you will have new Smart Things in your mobile app that correspond to your devices in definedZones.
* Now you can click "listen to vista" and it should run. If you open doors or windows with sensors it should send the events to SmartThings and receive commands from SmartThings

### Ways To Use ###

You can continue to use your alarm system normally and simply use the Vista sensors with SmartThings to get notifications of open doors or windows and for motion events. 

You can also have SmartThings automatically arm your alarm when everyone leaves and disarm it when someone comes home. You can have SmartThings notify you when the alarm becomes active. This is probably best if you use a company like ADT for your alarm service.

Alternatively, you can use a SmartApp like "Smart Alarm" and leave the Vista disarmed and allow Smart Alarm to activate your alarm. This gives great flexibility but since SmartThings currently requires all events to travel to the SmartThings cloud for processing it is possible that alarms can be delayed or never happen at all if SmartThings is experiencing a processing problem.

### Future Work ###

* allow mac configuration without having to edit source
* possibly show current status of sensors on mac and allow commands
* possibly do oauth on the mac side

### Security Concerns ###

When SmartThings sends a command to VistaThing it's it over unencrypted HTTP with no authentication. While this won't expose your alarm code, it does potentially allow anyone with access to your network the ability to disarm your alarm without a code. You have been warned. On a secure home network the risk seems minimal but it is non-zero.